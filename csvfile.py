import os
import pandas as pd
from langchain.agents import create_csv_agent
from langchain.llms import OpenAI

os.environ["OPENAI_API_KEY"]

def get_agent(file_path):
    if file_path.split(".")[-1] == "csv":
        agent = create_csv_agent(OpenAI(temperature=0), file_path, verbose=True)
    else:
        data = pd.read_excel(file_path, engine="openpyxl")
        data.to_csv("output_csv.csv", index=False)
        agent = create_csv_agent(OpenAI(temperature=0), "output_csv.csv", verbose=True)
    return agent


def get_answer(file_path, question):
    print("question", f": {question}")
    agent = get_agent(file_path)
    return agent.run(question)
